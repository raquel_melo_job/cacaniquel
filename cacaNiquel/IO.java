package com.company.dia105.cacaNiquel;

import java.util.ArrayList;
import java.util.Scanner;

public class IO {
    Scanner in;

    public IO() {
        this.in = new Scanner(System.in);
    }

    public void exibirResultado(int soma){
        System.out.println("Sua pontuação é: "+soma);
    }

    public void exibirSlots(ArrayList<String> resultado){
        System.out.println(resultado);
    }
}
