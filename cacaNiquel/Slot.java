package com.company.dia105.cacaNiquel;

public enum Slot {
    BANANA(10),
    FRAMBOESA(50),
    MOEDA(100),
    SETE(300);

    private int pontuacao;

    Slot(int pontuacao) {
        this.pontuacao = pontuacao;
    }

    public int getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }
}
