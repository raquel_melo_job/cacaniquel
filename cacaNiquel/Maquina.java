package com.company.dia105.cacaNiquel;

import java.util.ArrayList;
import java.util.Random;

public class Maquina {
//    HashMap<String,Integer> cacaNiquel;
    IO saida;

    public Maquina() {
        this.saida = new IO();
//        this.cacaNiquel = new HashMap<>();
    }

    public ArrayList<String> sortear(int quantSlots){
        Random gerador = new Random();
        Slot[] slots = Slot.values();

        ArrayList<String> sorteados = new ArrayList<>();

        for (int i=0;i<quantSlots;i++){
            int indiceSorteado = gerador.nextInt(slots.length);
            sorteados.add(slots[indiceSorteado].name());
        }

        return sorteados;
    }

    public void somaPontuacao(ArrayList<String> sorteados){
        int soma = 0;
        for (String sorteado : sorteados){
            soma+=Slot.valueOf(sorteado).getPontuacao();
        }
        this.saida.exibirResultado(soma);
    }


    public void montarResultado(ArrayList<String> sorteados) {
        ArrayList<String> temp = new ArrayList<>();
        for (String sorteado : sorteados){
            temp.add(sorteado+ " - "+Slot.valueOf(sorteado).getPontuacao());
        }
        this.saida.exibirSlots(temp);
    }

    public void adicionaSlot(){

    }
}
