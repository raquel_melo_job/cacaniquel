package com.company.dia105.cacaNiquel;

import java.util.ArrayList;

public class Jogador {
    private String nome;

    public Jogador(String nome) {
        this.nome = nome;
    }

    public void jogar(){
        Maquina m = new Maquina();
        ArrayList<String> sorteados = m.sortear(5);
        m.montarResultado(sorteados);
        m.somaPontuacao(sorteados);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
